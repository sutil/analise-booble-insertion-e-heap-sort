package algoritmo;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

public class BoobleSortTest {
	
	@Test
	public void test(){
		Integer[] vetor = {5,4,3,2,1};
		
		Integer[] ordenado = new BoobleSort().sort(vetor);
		
		assertArrayEquals(new Integer[]{1,2,3,4,5}, ordenado);
	}
	
	

}
