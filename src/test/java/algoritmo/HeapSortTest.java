package algoritmo;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

public class HeapSortTest {
	
	@Test
	public void test(){
		Integer[] lista = {1,2,3};
		
		Integer[] ordenado = new HeapSort().sort(lista);
		assertArrayEquals(new Integer[]{1,2,3}, ordenado);
	}
	
	@Test
	public void test2(){
		Integer[] lista = {4,3,2,1};
		
		Integer[] ordenado = new HeapSort().sort(lista);
		assertArrayEquals(new Integer[]{1,2,3,4}, ordenado);
	}
	
	@Test
	public void test3(){
		Integer[] lista = {4,3,7,1,9,2};
		
		Integer[] ordenado = new HeapSort().sort(lista);
		assertArrayEquals(new Integer[]{1,2,3,4,7,9}, ordenado);
	}


}
