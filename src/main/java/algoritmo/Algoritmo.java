package algoritmo;

public interface Algoritmo {
	
	public Integer[] sort(Integer[] entrada);

	public String getNome();

}
