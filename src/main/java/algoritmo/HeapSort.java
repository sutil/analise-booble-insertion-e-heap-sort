package algoritmo;

import java.util.Arrays;

public class HeapSort implements Algoritmo{

	Integer[] A;
	
	@Override
	public String getNome() {
		return "Heap Sort";
	}

	public HeapSort() {
	}

	public Integer[] sort(Integer[] entrada) {
		this.A = entrada;
		buildMaxHeap();
		int tamanho = A.length;

		for (int i = A.length - 1 ; i >= 0; i--) {
			troca(0, i);
			tamanho--;
			Integer[] Areduzido = Arrays.copyOf(A, tamanho);
			maxHeapify(Areduzido, 0);
			for(int p = 0; p < Areduzido.length ; p ++){
				A[p] = Areduzido[p];
			}
		}
		return A;
	}
	

	private void buildMaxHeap() {
		int tamanho = A.length;
		//calcular comprimento
		// lg n + 1 // elevalado a 2  // -1
		
		
		
		int log = (int)log2(tamanho) + 1;
		int comprimento = (int)Math.pow(2, log) - 1;
		
		for (int i = (comprimento / 2) - 1; i >= 0; i--)
			maxHeapify(A, i);
	}

	private double log2(int length) {
		return Math.log(length) / Math.log(2);
		
	}

	private void maxHeapify(Integer[] A, int i) {
		int atual = i;
		int e = esquerda(atual);
		int d = direita(atual);
		
		int maior = maior(A, atual, e, d);

		if (maior != atual) {
			troca(A, atual, maior);
			maxHeapify(A, maior);
		}

	}

	private int maior(Integer[] A, int atual, int e, int d) {
		int maior = atual;
		if(e < A.length && A[e] > A[maior])
			maior = e;
		if(d < A.length && A[d] > A[maior])
			maior = d;
		
		return maior;
	}

	private void troca(int i, int j) {
		Integer aux = A[i];
		A[i] = A[j];
		A[j] = aux;
	}
	
	private void troca(Integer[] A, int i, int j) {
		Integer aux = A[i];
		A[i] = A[j];
		A[j] = aux;
	}

	private int direita(int i) {
		if(i > 0)
			return i * 2 + 1;
		
		return 2;
	}

	private int esquerda(int i) {
		if(i > 0)
			return i * 2;
		return 1;
	}
}
