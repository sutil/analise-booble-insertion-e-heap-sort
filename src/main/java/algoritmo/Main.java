package algoritmo;

import static java.lang.String.format;

import java.time.Duration;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;

public class Main {
	
	public static void main(String[] args){
		
		for(Algoritmo algoritmo: getAlgoritmos())
			executa(algoritmo);
		
	}
	
	private static void executa(Algoritmo algoritmo) {
		Long tempoDeExecucao = 0L;
		int tamanhoDaEntrada = 10000;
		
		while (tempoDeExecucao < 1){
			tempoDeExecucao = 0L;
			print("=========================== %s ==============================", algoritmo.getNome());
			
			print("criando entrada de tamanho %d.", tamanhoDaEntrada);
			Integer[] entrada = getEntrada(tamanhoDaEntrada);
			
			print("Iniciando execução para entrada de tamanho %d.", tamanhoDaEntrada);
			LocalTime ini = LocalTime.now();
			algoritmo.sort(entrada);
			LocalTime fim = LocalTime.now();
			
			Duration duration = Duration.between(ini,fim);
			tempoDeExecucao = duration.get(ChronoUnit.SECONDS);
			
			print("Fim da execução do algoritmo %s para entrada de tamanho %d.", 
					algoritmo.getNome(), tamanhoDaEntrada);
			
			print("================Tempo de execução: %d segundos.===============\n\n\n", tempoDeExecucao);
		    tamanhoDaEntrada += 10000;
		}
		
		print("===================== FIM DA Execução do %s =================", algoritmo.getNome());
		print("A maior entrada que foi ordenada em menos de um minuto tem tamanho %d", tamanhoDaEntrada-20000);
		print("============================================================%s\n\n\n\n\n\n\n\n\n\n","=");
		
	}
	
	private static void print(String msg, Object... args){
		System.out.println(format(msg, args));
	}

	private static Integer[] getEntrada(int tamanho){
		Integer[] entrada = new Integer[tamanho]; 
		for(int i = 0 ; i < tamanho ; i++)
			entrada[i] = i;
		
		return entrada;
	}
	
	private static List<Algoritmo> getAlgoritmos(){
		return Arrays.asList(
				new BoobleSort(), 
				new InsertionSort(), 
				new HeapSort());
	}

}
