package algoritmo;

public class InsertionSort implements Algoritmo{
	
	private Integer[] lista;
	
	@Override
	public String getNome() {
		return "Insertion Sort";
	}
	
	public InsertionSort() {
	}

	public Integer[] sort(Integer[] entrada) {
		this.lista = entrada;
		Integer atual;

		for (int i = 0; i < lista.length; i++) {
			atual = lista[i];
			int j = i - 1;

			while ((j >= 0) && (atual < lista[j])) {
				lista[j + 1] = lista[j];
	            j = j - 1;
			}
	    
			lista[j + 1] = atual;
		}
		return lista;
	}

}
